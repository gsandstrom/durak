# Durak
Durak is a fun game for fun people.

## Dev instructions

### Installation

##### Ubuntu

*  Install [`node.js`](https://nodejs.org/en/) min version 12.x (I use [`n`](https://github.com/tj/n) to manage node versions on ubuntu). This should also install `npm`.
*  `git clone git@bitbucket.org:gsandstrom/durak.git`
*  `cd durak`
*  `npm ci`
*  (Recommended but optional) Install githooks `cp githooks/* .git/hooks/.`

### Running
My workflow has been to spin up `npm run webpack -- -w` and `npm run start` in separate processes. `npm run webpack -- -w` command watches for react / tsx files and rebuilds them on change. `npm run start` runs the express server ([default port: 3000](http://localhost:3000)) and needs to be manually restarted on file change. (TODO: setup watcher?)

More details:

`npm run start` runs `tslint` (linter and style checks), `tsc` (typescript compile), and run the express server defined in `/bin/www`.

`npm run webpack` compiles client `tsx` files down to js for express to serve.

See the `scripts` section of `package.json` for full list of `npm run` commands.

### Overview
Project uses [`npm`](https://www.npmjs.com/) for package management and was generated with [`express-generator`](https://expressjs.com/en/starter/generator.html). Client written with [react](https://reactjs.org/). Most code is written in [typescript](https://www.typescriptlang.org/), lives in the `/src` directory, and is compiled into the `/dist` directory. Code is organized into `src/client`, `src/server`, and shared `src/common` interfaces and helper functions.

### Tests
`npm run test` to run tests (currently only unit tests for `common` code). Tests are written with [jest](https://jestjs.io/) and live in `/tests`.

To run an individual test (useful to distinguish debug logging), use `-t` flag, e.g. `npm run test -- -t "test-name"`.

Jest can generate a test coverage report, pass it the `--coverage` flag thusly: `npm run test -- --coverage`. In addition to a summary in stdout, it will generate a `coverage/` directory with an interactive html report (defaults to `/path/to/repo/coverage/lcov-report/index.html`)

### npm shenanigans

#### clean install of npm modules
For first time dev space setup **or** after merging changes to `package.json` or `package-lock.json`, use `npm ci`. This will do a fresh install and build of `node_modules/` reading precise versions from `package-lock.json`, and cannot alter `package.json` or `package-lcok.json`.

#### installing new dependencies
If you need to add a dependency, use `npm install [package]` or `npm install --save-dev [package]`. This adds the dependency to `package.json` and makes changes to `packaget-lock.json`.

#### bumping package versions
To bump versions, you can directly edit `package.json` to require the new version then `rm -rf node_modules/ && npm install`, or try `npm udpate` to automatically get the latest. This will also update `package-lock.json` - make sure to commit both files and test that `npm ci` still succeeds.

## Deploying to production
tbd