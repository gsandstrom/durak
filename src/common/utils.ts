import MersenneTwister from 'mersenne-twister';

export function shuffle(array: any[], seed: number): void {
  const generator = new MersenneTwister(seed);
  let i = array.length;
  while(i > 0){
    const randomIndex = Math.floor(generator.random() * i);
    i -= 1;
    const tempCard = array[i];
    array[i] = array[randomIndex];
    array[randomIndex] = tempCard;
  }
};