export const FaceValues: {[name: string]: number} = {
  'TWO': 2,
  'THREE': 3,
  'FOUR': 4,
  'FIVE': 5,
  'SIX': 6,
  'SEVEN': 7,
  'EIGHT': 8,
  'NINE': 9,
  'TEN': 10,
  'JACK': 11,
  'QUEEN': 12,
  'KING': 13,
  'ACE': 14
}

export const Suits: {[name: string]: string} = {
  'SPADES': 'S',
  'CLUBS': 'C',
  'HEARTS': 'H',
  'DIAMONDS': 'D'
}

export interface Card {
  id: number;
  value: number;
  suit: string;
  img?: string;
}

export interface CardStack {
  bottom: Card;
  top?: Card;
}

export interface Player {
  id: number;
  name: string;
  hand: Card[];
}

export interface GameState {
  deck: Card[];
  discards: number;
  pantCard: Card;
  board: CardStack[];
  players: Player[];
  // round leader is the player who gets first attack in a round
  roundLeaderID: number;
  defenderID: number;
  firstPantCard: Card;
}

export interface PlayerGameState extends GameState {
  playerID: number;
  message?: MessagePacket;
}

// Actions are sent to game object
export interface Action {
  playerID: number;
}

export interface AttackAction extends Action {
  playedCard: Card;
}

export interface DefendAction extends Action {
  playedCard: Card;
  targetCard: Card;
}

export interface BounceAction extends Action {
  playedCard: Card;
}

// Requests are for client -> server interaction
export interface Request {
  type: string;
  userToken: string;
  gameID: string;
  bot?: boolean;
  action?: Action;
}

export interface JoinRequest extends Request {
  playerName: string;
}

export interface AttackRequest extends Request {
  action: AttackAction;
}

export interface DefendRequest extends Request {
  action: DefendAction;
}

export interface BounceRequest extends Request {
  action: BounceAction;
}

// Carry a compressed set of instructions for a
// message constructor on the front end
export interface MessagePacket {
  type: string;
  username?: string;
  card?: Card;
  target?: Card;
}
