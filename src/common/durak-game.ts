import { GameState, Player, Card, CardStack } from './durak-types';
import Debug from 'debug';
import rfdc from 'rfdc';
const clone = rfdc();

const debug = Debug('durak:game');

export class DurakGame {
  state: GameState;

  constructor(state: GameState) {
    this.state = clone(state);
  }

  gameState(): GameState {
    return clone(this.state);
  }

  // are these cards the same card
  cardEquals(cardA: Card, cardB: Card): boolean {
    // handle null or undefined
    return cardA && cardB && cardA.id === cardB.id;
  }

  // does the played card beat the target card with the given pant suit
  cardBeats(a: Card, b: Card): boolean {
    if((a.suit === b.suit) && (a.value > b.value)) {
      return true;
    } else if ((a.suit === this.pantSuit()) && (b.suit !== this.pantSuit())) {
      return true;
    }
    return false;
  }

  // is the card in the current player's hand
  inHand(target: Card, playerID: number): boolean {
    if(this.state.players[playerID].hand.find((card: Card) => this.cardEquals(card, target))) {
      return true;
    }
    return false;
  }

  handSize(playerID: number): number {
    return this.state.players[playerID].hand.length;
  }

  // is the card on the board and not already covered
  isOpen(card: Card): boolean {
    let found = false;
    this.state.board.forEach( (stack: CardStack) => {
      if (this.cardEquals(card, stack.bottom) && !stack.top) {
        found = true;
      }
    });
    return found;
  }

  // do any cards with the given value appear on the board
  isValueOnBoard(value: number): boolean {
    const findValue = (stack: CardStack) => {
      if (stack.bottom.value === value) {
        return true;
      } else if (stack.top) {
        if (stack.top.value === value) {
          return true;
        }
      }
      return false;
    };

    if (this.state.board.find(findValue)) {
      return true;
    }
    return false;
  }

  countOpenStacks(): number {
    // how many stacks don't have a top card
    return this.state.board.filter( (stack: CardStack) => { return stack.top === undefined }).length;
  }

  deckSize(): number {
    return this.state.deck.length;
  }

  discardPileSize(): number {
    return this.state.discards;
  }

  pantSuit(): string {
    return typeof this.state.pantCard === 'undefined' ?
      undefined :
      this.state.pantCard.suit;
  }

  getDefenderID(): number {
    return this.state.defenderID;
  }

  isDefending(playerID: number): boolean {
    return playerID === this.getDefenderID();
  }

  // id of current leader, or -1 if there isn't one (round is in progress, for example)
  getRoundLeaderID(): number {
    return (
      typeof this.state.roundLeaderID === 'undefined' ||
      this.state.board.length > 0
      ) ?
      -1 :
      this.state.roundLeaderID;
  }

  isLeading(playerID: number): boolean {
    return playerID === this.getRoundLeaderID();
  }

  isAttacking(playerID: number): boolean {
    return !this.isDefending(playerID);
  }

  nextPlayer(currentID: number): number {
    // return playerID of the next active player after currentID
    let nextID = currentID;
    do {
      nextID = this.mod((nextID + 1), this.state.players.length);
    } while (!this.isPlayerActive(nextID) && nextID !== currentID)
    return nextID;
  }

  prevPlayer(currentID: number): number {
    // return playerID of the previous active player before currentID
    let prevID = currentID;
    do {
      prevID = this.mod((prevID - 1), this.state.players.length);
    } while (!this.isPlayerActive(prevID) && prevID !== currentID)
    return prevID;
  }

  isPlayerActive(playerID: number): boolean {
    // player is out of the game if they have no cards and the deck is empty
    if (this.state.players[playerID].hand.length === 0 && this.deckSize() === 0) {
      return false;
    } else {
      return true;
    }
  }

  isGameOver(): boolean {
    // game is over when there's only one active player
    let activePlayerCount = 0;
    this.state.players.forEach((player: Player) => {
      if (this.isPlayerActive(player.id)) {
        activePlayerCount += 1;
      }
    });
    if (activePlayerCount <= 1) {
      return true;
    } else {
      return false;
    }
  }

  canAttack(card: Card, playerID: number): boolean {
    if (this.isDefending(playerID)) {
      debug('canAttack: you are the defender, and cannot attack');
      return false;
    } else if (this.state.players[this.getDefenderID()].hand.length <= this.countOpenStacks()) {
      debug('canAttack: the defender does not have enough cards to accept this attack');
      return false;
    } else if (!this.inHand(card, playerID)) {
      debug('canAttack: you do not have that card');
      return false;
    } else if (
      !this.isValueOnBoard(card.value) &&
      !this.isLeading(playerID)
    ) {
      debug('canAttack: cannot play a value that has not appeared yet');
      return false;
    }
    return true;
  }

  canDefend(played: Card, target: Card, playerID: number): boolean {
    if (!this.isDefending(playerID)) {
      debug('canDefend: you are not the current defender');
      return false;
    } else if (!this.inHand(played, playerID)) {
      debug('canDefend: played card is not in your hand');
      return false;
    } else if (!this.isOpen(target)) {
      debug('canDefend: target card is not open');
      return false;
    } else if (!this.cardBeats(played, target)) {
      debug('canDefend: [' + played + '] does not beat [' + target + ']');
      return false;
    }
    return true;
  }

  canBounce(card: Card, playerID: number): boolean {
    if (!this.isDefending(playerID)) {
      debug('canBounce: you are not the defending player');
      return false;
    } else if (!this.inHand(card, playerID)) {
      debug('canBounce: you do not have that card');
      return false;
    } else if (this.state.board.length === 0) {
      debug('canBounce: nothing has been played yet');
      return false;
    } else if (this.state.board.length !== this.countOpenStacks()) {
      debug('canBounce: a defense has already occurred');
      return false;
    } else if (card.value !== this.state.board[0].bottom.value) {
      debug('canBounce: bounce must be done with a card of the same value');
      return false;
    } else if (
      (this.state.board.length + 1) >
      this.state.players[this.nextPlayer(this.getDefenderID())].hand.length
    ) {
      debug('canBounce: next defending player does not have enough cards');
      return false;
    }
    return true;
  }

  canSurrender(playerID: number): boolean {
    if (!this.isDefending(playerID)) {
      debug('canSurrender: you are not the defending player');
      return false;
    } else if (this.state.board.length === 0) {
      debug('canSurrender: cannot surrender until someone attckas');
      return false;
    }
    return true;
  }

  // are all cards covered?
  canClear() {
    if(this.state.board.length === 0) {
      // can't clear an empty board
      return false;
    }
    const undefended = this.state.board.find((stack: CardStack) => {
      if(stack.top) {
        return false;
      } else {
        return true;
      }
    });
    if(undefended) {
      return false;
    } else {
      return true;
    }
  }

  mod(x: number, n: number): number {
    // https://maurobringolf.ch/2017/12/a-neat-trick-to-compute-modulo-of-negative-numbers/
    return (x % n + n) % n;
  }
}
