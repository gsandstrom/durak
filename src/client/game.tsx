import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { GameUI } from './components/durak-app';

ReactDOM.render(
  <GameUI compiler="TypeScript" framework="React" />,
  document.getElementById("root")
);
