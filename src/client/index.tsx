import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { HomepageUI } from './components/homepage';

ReactDOM.render(
  <HomepageUI compiler="TypeScript" framework="React" />,
  document.getElementById("root")
);
