import { DurakGame } from '../common/durak-game';
import { GameState, PlayerGameState, Card } from '../common/durak-types';

export class DurakClient extends DurakGame {
  state: PlayerGameState;

  constructor(state: PlayerGameState) {
    super(state);
    this.state.playerID = state.playerID;
  }

  gameState(): GameState {
    const state = super.gameState();
    return state;
  }

  update(newState: PlayerGameState): DurakClient {
    if(this.myID() >= 0 && this.myID() === newState.playerID) {
      // preserve old hand order, but remove cards that are no longer present
      //  and append cards that are new
      const oldHand = this.myHand();
      const newHand = newState.players[this.myID()].hand;
      const remainingCards = oldHand.filter((oldCard: Card) => {
        if(newHand.find((newCard: Card) => this.cardEquals(oldCard, newCard))) {
          return true;
        }
        return false;
      });
      const newCards = newHand.filter((newCard: Card) => {
        if(!oldHand.find((oldCard: Card) => this.cardEquals(oldCard, newCard))) {
          return true;
        }
        return false;
      });
      this.state = newState;
      this.state.players[this.myID()].hand = remainingCards.concat(newCards);
    } else {
      // if you are a spectator, or your playerID has changed, just update the state
      this.state = newState;
    }
    return this;
  }

  myHand(): Card[] {
    if (this.myID() >= 0) {
      return this.state.players[this.myID()].hand;
    } else {
      return [];
    }
  }

  inMyHand(target: Card): boolean {
    if(this.myHand().find((card: Card) => this.cardEquals(card, target))) {
      return true;
    }
    return false;
  }

  indexInHand(target: Card): number {
    return this.myHand().findIndex((card: Card) => this.cardEquals(card, target));
  }

  myID(): number {
    return this.state.playerID;
  }

  moveCards(slot: number, selectedCardIndex: number[]): DurakClient {
    // Collect the selected cards; drop slot number for each card left of the slot
    const selectedCards: Card[] = [];
    selectedCardIndex.sort();
    selectedCardIndex.forEach((selection: number) => {
      selectedCards.push(this.myHand()[selection]);
      if (selection < slot) {
        slot -= 1;
      }
    })

    // Remove them from the hand
    let idx = this.myHand().length;
    while (idx--) {
      if (selectedCardIndex.indexOf(idx) >= 0) { 
        this.myHand().splice(idx, 1);
      }
    }

    // Re-introduce them into the hand
    this.myHand().splice(slot, 0, ...selectedCards);
    return this
  }
}