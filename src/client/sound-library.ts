import UIfx, { config } from 'uifx';
let cardSmallAudio = require('./sounds/card-plop-small.mp3').default;
let cardBigAudio = require('./sounds/card-plop-big.mp3').default;
let dingAudio = require('./sounds/glass-ding.mp3').default;
let tableScrapeAudio = require('./sounds/table-scrape.mp3').default;
let knifeScrapeAudio = require('./sounds/knife-scrape.mp3').default;

class WrappedUIfx {
  enabled: boolean;
  fx: UIfx;

  constructor(file: string, config?: config) {
    this.fx = new UIfx(file, config);
    this.enabled = true;
  }

  enable(enable: boolean) {
    this.enabled = enable;
  }

  play(volume?: number) {
    if (this.enabled) this.fx.play(volume);
  }
}

class SoundLibrary {
  cardSmall: WrappedUIfx;
  cardBig: WrappedUIfx;
  notifyLeading: WrappedUIfx;
  clear: WrappedUIfx;
  notifyDefending: WrappedUIfx;

  constructor() {
    this.cardSmall = new WrappedUIfx(cardSmallAudio, { volume: 1 });
    this.cardBig = new WrappedUIfx(cardBigAudio, { volume: .8 });
    this.notifyLeading = new WrappedUIfx(dingAudio, { volume: .2 });
    this.clear = new WrappedUIfx(tableScrapeAudio, { volume: 1 });
    this.notifyDefending = new WrappedUIfx(knifeScrapeAudio, { volume: .2 });
  }

  enable(enable: boolean) {
    this.cardSmall.enable(enable);
    this.cardBig.enable(enable);
    this.notifyLeading.enable(enable);
    this.clear.enable(enable);
    this.notifyDefending.enable(enable);
  }
}

export const sounds = new SoundLibrary();