import { UIPosition } from './ui-types';

const cardWidth = 75;       // Card Width - should be consistent with .card in the CSS
const cardHeight = 130;     // Card Height - should be consistent with .card in the CSS

interface HandStyle {
  height: string;
}

interface HandPosition {
  cardPositions: UIPosition[];
  slotPositions: UIPosition[];
  handStyle: HandStyle;
}

const arrowPosition = (uiPosition: UIPosition, side: string): UIPosition => {
  // From a Card's UIPosition, create the SlotArrow's UIPosition
  // to hang out over the left top corner or the center
  let arrowPadUp = 16;       // Sets how much vertical space is between the arrows and the cards
  let arrowPadLeft = 5;      // Sets a horizental offset for the arrows from the left edge of the cards

  // Calculate the offset of the arrow using the card's UIPosition as reference
  let cardRadius = 0;
  if (side === 'left') {
    cardRadius = Math.sqrt((0.5 * cardWidth + arrowPadLeft)**2 + ((0.5 * cardHeight + arrowPadUp)**2));
  } else {
    cardRadius = 0.5 * cardHeight + arrowPadUp;
  }
  let gamma = Math.atan2(0.5 * cardHeight + arrowPadUp, 0.5 * cardWidth + arrowPadLeft);
  let phi = uiPosition.rotation * Math.PI / 180;
  let deltaX = 0;
  let deltaY = 0;
  if (side === 'left') {
    deltaX = cardRadius * (Math.cos(gamma) - Math.cos(gamma + phi));
    deltaY = cardRadius * (Math.sin(gamma) - Math.sin(gamma + phi));
  } else {  // if not set to left side, assume arrow is over the center
    deltaX = cardRadius * Math.sin(phi)
    deltaY = cardRadius * (1 - Math.cos(phi));
  }

  let newPosition: UIPosition = {
    rotation: uiPosition.rotation,
    xPosition: uiPosition.xPosition + deltaX,
    yPosition: uiPosition.yPosition - arrowPadUp + deltaY,
  }
  if (side === 'left') {
    newPosition.xPosition -= arrowPadLeft;
  } else {
    newPosition.xPosition += 0.5 * cardWidth;
  }
  return newPosition;
}


export const calcHandPosition = (handLength: number): HandPosition => {
  // Doing the right thing and protecting the code, despite extra lines
  if (handLength < 1) {
    return {
      cardPositions: [],
      slotPositions: [],
      handStyle: {
        height: '0px',
      }
    }
  }

  let handPadding = 20;     // Hand Component Padding - should be consistent with .hand in the CSS
  
  // bunch of manual calibration to get the hand arc right
  let cardSpacing = .5;     // Percentage of the card width to space neighboring cards
  let arcRadius = handLength > 5 ? 100 * handLength - 200 : 400;   // Heuristic function to flatten arc
  
  // calculate a range of rotation angles
  let theta = Math.atan2(cardWidth * cardSpacing, arcRadius) * 180 / Math.PI;
  let startAngle = -0.5 * theta * (handLength - 1);
  let degreeArray: number[] = [];
  for (let i = 0; i < handLength; i++) {
    degreeArray.push(Math.round(100 * (startAngle + (theta * i))) / 100);
  }

  // Construct the array of UIPositions
  let maxX = arcRadius * Math.sin(Math.max(...degreeArray) * Math.PI / 180);
  let uiPosArray: UIPosition[] = degreeArray.map((deg: number): UIPosition => {
    return {
      rotation: deg,
      xPosition: cardWidth + maxX + arcRadius * Math.sin(deg * Math.PI / 180),
      yPosition: handPadding + arcRadius * (1 - Math.cos(deg * Math.PI / 180))
    }
  })

  // Construct the array of Slot UIPositions, including the right-most slot arrow
  let slotPosArray: UIPosition[] = uiPosArray.map((uiPos: UIPosition): UIPosition => {
    return arrowPosition(uiPos, 'left');
  })
  slotPosArray.push(arrowPosition(uiPosArray[uiPosArray.length - 1], 'right'))

  // Style the hand component to be tall enough to hold the cards
  let maxY = arcRadius * (1 - Math.cos(Math.max(...degreeArray) * Math.PI / 180));
  let handStyle: HandStyle = {
    height: cardHeight + maxY + 'px',
  }

  return {
    cardPositions: uiPosArray,
    slotPositions: slotPosArray,
    handStyle: handStyle,
  }
}