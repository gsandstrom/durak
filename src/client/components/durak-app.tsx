import React, { Component, SyntheticEvent } from 'react';
import Debug from 'debug';
import Cookies from 'js-cookie';
import Button from '@material-ui/core/Button';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import Typography from '@material-ui/core/Typography';
import LoginDialogUI from './login-dialog';
import { CardUI, SlotArrowUI } from './card-ui';
import PrettyCardNameUI from './pretty-card';
import LogConsoleManager from './log-console';
import { calcHandPosition } from '../hand-angles';
import { PlayerGameState, Card, CardStack } from '../../common/durak-types';
import { Request } from '../../common/durak-types';
import { Action, AttackAction, DefendAction, BounceAction } from '../../common/durak-types';
import { UIState } from '../ui-types';
import { DurakClient } from '../durak-client';
import { sounds } from '../sound-library';
import { PlayerBar } from './player-bar';
import '../../../public/stylesheets/durak-client.css';

const debug = Debug('durak-app');

function extractGameName(path: string): string {
  // remove potential trailing '/' and return only end of path
  return path.replace(/\/?$/, '').split('/').slice(-1)[0];
}

interface UIGame {
  username?: string;
  userToken: string;
  gameID: string;
  game: DurakClient;
  ui?: UIState;
  enableSound: boolean;
  consoleManager: LogConsoleManager;
  clearTimer: number;
  clearCount: number;
}

export class GameUI extends Component<any, UIGame> {
  constructor(props: any) {
    super(props);
    this.state = {
      username: undefined,
      userToken: Cookies.get('userToken'),
      game: undefined,
      gameID: extractGameName(window.location.pathname),
      ui: {
        selected: [],
        hovered: null
      },
      enableSound: true,
      consoleManager: new LogConsoleManager(),
      clearTimer: undefined,
      clearCount: undefined
    };
  }

  componentDidMount() {
    socket.onopen = () => {
      socket.onmessage = (message: any) => {
        if(typeof message.data === 'string') this.updateGameState(JSON.parse(message.data) as PlayerGameState);
      };
      // fire off a connect request to register userToken
      this.emit('register');
    };
  }

  updateGameState =  (updatedGameState: PlayerGameState)=> {
    // update the game state
    let game = this.state.game;
    if(typeof this.state.game === 'undefined') {
      game = new DurakClient(updatedGameState);
    }
    game.update(updatedGameState);

    // reset ui selection if the selected card(s) is no longer in hand
    let uiReset = this.state.ui;
    if(uiReset.selected) {
      let idx = uiReset.selected.length;
      while (idx--) {
        if (!game.inHand(uiReset.selected[idx], game.myID())) { 
          uiReset.selected.splice(idx, 1);
        }
      }
    }

    // add any incoming messages to the message queue
    let consoleManager = this.state.consoleManager;
    if (updatedGameState.message !== undefined) {
      // update the action log and set the message to fade in
      let messageIndex = consoleManager.update(updatedGameState.message);
      setTimeout(() => this.fadeInMessage(messageIndex), 10);

      // play a sound for certain messages
      const type = updatedGameState.message.type;
      // if (type === 'join-spectator')
      if (
        (type === 'attack' || type === 'defend') &&
        updatedGameState.message.username !== this.state.username // TODO: send playerID not name
      ) {
        // only trigger sound if someone else played the card
        sounds.cardSmall.play();
      }
      if (type === 'clear' || type === 'surrender') sounds.clear.play();
      if (type === 'surrender' || type === 'clear' || type === 'start') {
        // play a notification sound for leader and defender if board just cleared
        if (this.state.game.isLeading(this.state.game.myID())) {
          sounds.notifyLeading.play();
        }
        if (this.state.game.isDefending(this.state.game.myID())) {
          sounds.notifyDefending.play();
        }
      }
    }

    // deal with the clear timer for the defending player
    if(game.canClear() && game.isDefending(game.myID())) {
      // cancel any running timer
      window.clearInterval(this.state.clearTimer);
      // set a new timer to count down to zero
      const timer = window.setInterval(() => {
        const count = this.state.clearCount;
        if (count > 0) {
          this.setState({clearCount: count-1});
        } else {
          // stop the countdown at 0
          window.clearInterval(this.state.clearTimer);
        }
      }, 1000);
      this.setState({clearCount: 5, clearTimer: timer});
    } else {
      window.clearInterval(this.state.clearTimer);
      this.setState({clearCount: undefined, clearTimer: undefined});
    }

    // update state
    this.setState({
      game: game,
      ui: uiReset,
      consoleManager: consoleManager,
    });
  };

  fadeInMessage = (index: number) => {
    // Fade in by making active
    let consoleManager = this.state.consoleManager;
    consoleManager.activate(index);
    this.setState({consoleManager: consoleManager});
  }

  // for some requests, you don't need to pass an action argument
  emit(type: string, action?: Action): void {
    const request: Request = {
      type,
      userToken: this.state.userToken,
      gameID: this.state.gameID,
      action
    };
    socket.send(JSON.stringify(request));
  }

  handClicked = (_event: SyntheticEvent) => {
    // If the hand is clicked without a card/slot click, deselect all cards.
    // This requires us to put an event.stopPropagation() in the card/slot click methods.
    debug('hand clicked: deselecting cards');
    this.setState({ui: {selected: []}});
  }

  cardClicked = (event: SyntheticEvent, clicked: Card) => {
    event.stopPropagation();
    debug("click event! [" + clicked + "]");
    const selected = this.state.ui.selected;
    const game = this.state.game;
    const myID = game.myID();
    if (selected.length === 0) {
      // only select a card if it's in your hand (don't select board cards)
      if (game.inMyHand(clicked)) {
        debug("first selection");
        this.setState({ui: {selected: [clicked]}});
      }
    } else if (game.inMyHand(clicked) && selected.every((selection: Card) => clicked.id !== selection.id)) {
      // if clicking a card in hand not currently selected. add to selection if same card value
      if (clicked.value === selected[0].value) {
        debug("adding selection");
        this.setState({ui: {selected: selected.concat([clicked])}});
      } else {
        debug("changing selection within hand");
        this.setState({ui: {selected: [clicked]}});
      }
    } else if (selected.some((selection: Card) => clicked.id === selection.id)) {
      // if clicking a card currently selected
      debug("deselecting");
      let deselectIndex = selected.findIndex((selection: Card) => clicked.id === selection.id);
      selected.splice(deselectIndex, 1);
      this.setState({ui: {selected: selected}});
    } else if (game.isOpen(clicked) && selected.length === 1 && game.canDefend(selected[0], clicked, myID)) {
      debug("defending");
      const defense: DefendAction = {
        playerID: myID,
        playedCard: selected[0],
        targetCard: clicked
      };
      this.emit('defend', defense);
      sounds.cardBig.play();
    }
  }

  slotClicked = (event: SyntheticEvent, slot: number, selectedCardIndex: number[]) => {
    event.stopPropagation();
    debug('moving card within hand');
    let game = this.state.game;
    this.setState({game: game.moveCards(slot, selectedCardIndex), ui: {selected: []}});
  }

  boardClicked = () => {
    debug("click event! [board clicked]");
    const selected = this.state.ui.selected;
    const game = this.state.game;
    const myID = game.myID();
    if (selected.length === 0) {
      // do nothing
    } else {
      let barrage = false;
      if (game.canAttack(selected[0], myID)) {
        debug("attacking");
        const attack: AttackAction = {
          playerID: myID,
          playedCard: selected[0]
        };
        this.emit('attack', attack);
        sounds.cardBig.play();
        barrage = true;
      } else if (game.canBounce(selected[0], myID)) {
        debug("bouncing");
        const bounce: BounceAction = {
          playerID: myID,
          playedCard: selected[0]
        };
        this.emit('bounce', bounce);
        sounds.cardBig.play();
        barrage = true;
      }
      // Try and sequentially attack with the remaining selected cards.
      // This relies on the back-end rejecting attacks if they are invalid
      if (barrage) {
        selected.slice(1).forEach((card) => {
          debug("attacking");
          const attack: AttackAction = {
            playerID: myID,
            playedCard: card
          };
          this.emit('attack', attack);        
        })
      }
    }
  }

  cardHovered = (card: Card) => {
    this.setState({ui: {hovered: card}});
  }

  surrender = (event: SyntheticEvent) => {
    this.emit('surrender');
    event.preventDefault();
  }

  clearBoard = (event: SyntheticEvent) => {
    this.emit('clear');
    event.preventDefault();
  }

  startGame = (event: SyntheticEvent) => {
    this.emit('start');
    event.preventDefault();
  }

  undo = (event: SyntheticEvent) => {
    this.emit('undo');
    event.preventDefault();
  }

  redo = (event: SyntheticEvent) => {
    this.emit('redo');
    event.preventDefault();
  }

  addBot = () => {
    this.emit('addBot');
  }

  handleSoundChange = (event: { target: HTMLInputElement }) => {
    sounds.enable(event.target.checked);
    this.setState({enableSound: event.target.checked});
  }

  getBoardMessage = (): string => {
    const game = this.state.game;
    let message = '';
    if(typeof this.state.game !== 'undefined') {
      const roundLeaderID = game.getRoundLeaderID();
      const defenderID = game.getDefenderID();
      if (game.isGameOver()) {
        message = 'Game Over';
      } else if (game.myID() < 0) {
        message = 'You are spectating';
      } else if (!game.isPlayerActive(game.myID())) {
        message = 'You are not the Durak!'
      } else if (game.isLeading(game.myID())) {
        // you are the leader, it's your turn to play
        const defenderName = game.state.players[defenderID].name;
        message = 'It is your turn to lead against ' + defenderName;
      } else if (roundLeaderID >= 0) {
        // waiting for someone else to play
        const leaderName = game.state.players[roundLeaderID].name;
        let defenderName = game.state.players[defenderID].name;
        if (defenderName === game.state.players[game.myID()].name) defenderName = 'you';
        message = 'Waiting for ' + leaderName + ' to lead against ' + defenderName;
      } else if (game.isDefending(game.myID())) {
        // you are the defending
        message = 'You are defending';
      } else if (game.handSize(defenderID) > game.countOpenStacks()) {
        // there is space to attack
        const defenderName = this.state.game.state.players[defenderID].name;
        message = 'You are free to attack ' + defenderName;
      } else {
        // otherwise, defender has no more room
        const defenderName = this.state.game.state.players[defenderID].name;
        message = 'Waiting for ' + defenderName + ' to finish defending or pick up';
      }
    }
    return message;
  }

  renderGame = () => {
    const game = this.state.game;
    const myHand = game.myHand();
    const handPosition = calcHandPosition(myHand.length);
    return (
      <div className="game">
        <PlayerBar game={game}/>
        <div className="board" onClick={() => {this.boardClicked()}}>
          {game.state.board.map((stack: CardStack, index: number) => {
            return (
              <span className="stack" key={index}>
                <CardUI isBot={true} key={0} onBoard={true} card={stack.bottom} ui={this.state.ui} onClick={this.cardClicked} />
                {
                  stack.top ? 
                  <CardUI isBot={false} key={1} onBoard={true} card={stack.top} ui={this.state.ui} onClick={this.cardClicked} /> :
                  <span></span>
                }
              </span>
            )
          })}
          <div className="boardMessage">
            <Typography variant='h4'>{this.getBoardMessage()}</Typography>
          </div>
          <div className="boardControls">
            { this.state.game.canSurrender(this.state.game.myID()) && !this.state.game.canClear() ?
              <Button variant='contained' color='secondary' onClick={this.surrender}>
                Surrender
              </Button> :
              <span></span>
            }
            { typeof this.state.clearCount !== 'undefined' ?
              <Button variant='contained' color='primary' onClick={this.clearBoard} disabled={this.state.clearCount > 0}>
                {this.state.clearCount === 0 ? "Clear board" : this.state.clearCount.toString()}
              </Button> :
              <span></span>
            }
            { this.state.game.isGameOver() ?
              <Button variant='contained' color='primary' size='large' onClick={this.startGame}>
                Start new game
              </Button> :
              <span></span>
            }
          </div>
        </div>
        {(myHand.length === 0) ?
          <div></div> :
          <div className="hand" style={handPosition.handStyle} onClick={this.handClicked}>
            {myHand.map((card: Card, index: number) => {
              let uiPosition = handPosition.cardPositions[index];
              let slotPosition = handPosition.slotPositions[index];
              return [
                <SlotArrowUI
                  key={"slot" + index}
                  cards={myHand}
                  slotIndex={index}
                  uiPosition={slotPosition}
                  ui={this.state.ui}
                  onSlotClick={this.slotClicked}
                />,
                <CardUI
                  key={"card" + index}
                  card={card}
                  uiPosition={uiPosition}
                  ui={this.state.ui}
                  onClick={this.cardClicked}
                />
              ];
            })}
            <SlotArrowUI
              key={"slot" + myHand.length}
              cards={myHand}
              uiPosition={handPosition.slotPositions[handPosition.slotPositions.length - 1]}
              slotIndex={myHand.length}
              ui={this.state.ui}
              onSlotClick={this.slotClicked}
            />
          </div>
        }
        <div>
          <div className="controls">
            <Typography component={'span'} variant='body1'>
              <p>Cards left in deck: {game.deckSize()}</p>
              <p>Cards in discard pile: {game.discardPileSize()}</p>
              <p>Pant Card: <PrettyCardNameUI card={game.state.pantCard}/></p>
            </Typography>
            <Button onClick={this.undo}>Undo</Button>
            <Button onClick={this.redo}>Redo</Button>
            <Button onClick={this.startGame}>Restart Game</Button>
            <Button onClick={this.addBot}>Add Bot</Button>
            <FormGroup>
              <FormControlLabel
                control={<Switch onChange={this.handleSoundChange} checked={this.state.enableSound}/>}
                label="Enable Sound"
              />
            </FormGroup>
          </div>
          {this.state.consoleManager.render()}
        </div>
      </div>
    );
  }

  renderLogin = () => {
    return (
      <div className="login">
        <LoginDialogUI userToken={this.state.userToken} socket={socket} gameID={this.state.gameID} />
      </div>
    );
  }

  render() {
    if (typeof this.state.game === 'undefined') {
      return(this.renderLogin());
    } else {
      return(this.renderGame());
    }
  }
}

const wsProtocol = window.location.protocol === 'http:' ? 'ws:///' : 'wss:///';
const socket = new WebSocket(wsProtocol + window.location.hostname + ':' + window.location.port, 'durak-protocol');
