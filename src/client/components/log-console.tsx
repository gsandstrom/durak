import React from 'react';
import Typography from '@material-ui/core/Typography';
import PrettyCardNameUI from './pretty-card';
import { MessagePacket } from '../../common/durak-types';

interface LogMessagePacket extends MessagePacket {
  active: boolean;
  redacted: boolean;
}

export default class LogConsoleManager {
  queue: Map<number, LogMessagePacket>;
  index: number;
  maxDisplay: number;

  constructor () {
    this.queue = new Map<number, LogMessagePacket>();
    this.index = -1;
    this.maxDisplay = 10;
  }

  // overall update method: return the index of the incoming MessagePacket
  update(newPacket: MessagePacket): number {
    let newLogPacket: LogMessagePacket = {
      ...newPacket,
      active: false,
      redacted: false,
    }

    // if we get a new game message, refresh things
    if (newLogPacket.type === 'start') {
      this.index = -1;
      this.queue = new Map<number, LogMessagePacket>();
    }

    // append the packet to the queue
    this.index++;
    this.queue.set(this.index, newLogPacket);
    

    // based on the action, redact/delete older messages
    const retireTypes = ['surrender', 'clear'];
    if (retireTypes.includes(newLogPacket.type)) {
      [...this.queue.keys()].forEach((index) => { this.retire(index) })
    }

    // clean up the queue if it's getting too big
    if (this.queue.size > 2 * this.maxDisplay) { this.clean() }

    return this.index;
  }

  // retire an entry, either by redacting it or deleting it
  retire(index: number) {
    let packet = this.queue.get(index);
    const redactTypes = ['first-attack', 'bounce', 'start'];
    const deleteTypes = ['attack', 'defend', 'undo', 'redo'];
    if (redactTypes.includes(packet.type)) {
      packet.redacted = true;
    } else if (deleteTypes.includes(packet.type)) {
      this.queue.delete(index);
    }
  }

  // delete messages when the queue gets too long
  clean() {
    const numDelete = this.queue.size - 2 * this.maxDisplay;
    const toDelete = [...this.queue.keys()].sort().slice(0, numDelete);
    toDelete.forEach((index) => { this.queue.delete(index) })
  }

  // publish the top messages
  render() {
    return (
      <div className="console">
        <Typography component={'span'} variant='body1'>
          {[...this.queue.entries()].reverse().slice(0, this.maxDisplay).map(([index, packet]) => { 
            const className = `message ${packet.active ? 'show' : 'hide'}`
            return (
              <p key={index} className={className}>
                &#x2022; {this.unpack(packet)}
              </p>
            )
          })}
        </Typography>
      </div>
    );
  }

  // unpack a given message packet
  unpack(packet: LogMessagePacket) {
    if (packet.type === 'attack') {
      return <span> <b>{packet.username}</b> attacked with <PrettyCardNameUI card={packet.card}/> </span>
    } else if (packet.type === 'first-attack') {
      if (packet.redacted) {
        return <span> <b>{packet.username}</b> led the attack </span>
      } else {
        return <span> <b>{packet.username}</b> led the attack with <PrettyCardNameUI card={packet.card}/> </span>
      }
    } else if (packet.type === 'defend') {
      return <span> <b>{packet.username}</b> defended <PrettyCardNameUI card={packet.target}/> with <PrettyCardNameUI card={packet.card}/> </span>
    } else if (packet.type === 'bounce') {
      if (packet.redacted) {
        return <span> <b>{packet.username}</b> bounced the attack </span>
      } else {
        return <span> <b>{packet.username}</b> bounced the attack with <PrettyCardNameUI card={packet.card}/> </span>
      }
    } else if (packet.type === 'surrender') {
      return <span> <b>{packet.username}</b> has surrendered </span>
    } else if (packet.type === 'clear') {
      return <span> <b>{packet.username}</b> successfully defended the board </span>
    } else if (packet.type === 'join-player') {
      return <span> <b>{packet.username}</b> has entered the fray! </span>
    } else if (packet.type === 'join-spectator') {
      return <span> <b>{packet.username}</b> is now spectating </span>
    } else if (packet.type === 'undo') {
      return <span> Reversing one game step... </span>
    } else if (packet.type === 'redo') {
      return <span> Replaying one game step... </span>
    } else if (packet.type === 'start') {
      if (packet.card !== undefined) {
        if (packet.redacted) {
          return <span> <b>{packet.username}</b> started off the game </span>
        } else {
          return <span> GAME ON! <b>{packet.username}</b> reveals <PrettyCardNameUI card={packet.card}/> and goes first </span>
        }
      } else {
        return <span> GAME ON! Fortune has chosen <b>{packet.username}</b> to lead the attack </span>
      }
    }
  }

  // activate a packet
  activate(index: number) {
    let packet = this.queue.get(index);
    if (packet !== undefined) {
      packet.active = true;
    } 
  }
}
