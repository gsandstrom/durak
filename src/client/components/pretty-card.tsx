import React, { Component } from 'react';
import { Card } from '../../common/durak-types';

const values: {[val: number]: string} = {
  1: "1",
  2: "2",
  3: "3",
  4: "4",
  5: "5",
  6: "6",
  7: "7",
  8: "8",
  9: "9",
  10: "10",
  11: "J",
  12: "Q",
  13: "K",
  14: "A"
}
const suits: {[char: string]: string} = {
  "D": "\u2666",
  "C": "\u2663",
  "H": "\u2665",
  "S": "\u2660"
}
const color: {[char: string]: string} = {
  "D": "red",
  "C": "black",
  "H": "red",
  "S": "black"
}

interface PrettyCardNameUIProps {
  card?: Card;
}

export default class PrettyCardNameUI extends Component<PrettyCardNameUIProps> {
  render() {
    if (typeof this.props.card === 'undefined') {
      return(<span></span>);
    } else {
      return (
        <span className={color[this.props.card.suit]}>
          {values[this.props.card.value]} {suits[this.props.card.suit]}
        </span>
      )
    }
  }
}