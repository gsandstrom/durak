import React, { Component, ChangeEvent } from 'react';
import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import { JoinRequest } from '../../common/durak-types';
import '../../../public/stylesheets/login.css';

interface LoginDialogUIState {
  username: string;
  disabled: boolean;
}
  
interface LoginDialogUIProps {
  userToken: string;
  socket: WebSocket;
  gameID: string;
}

export default class LoginDialogUI extends Component<LoginDialogUIProps, LoginDialogUIState> {
  constructor(props: any) {
    super(props);
    this.state = {username: '', disabled: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event: ChangeEvent<HTMLInputElement>) {
    this.setState({username: event.target.value});
  }

  handleSubmit() {
    const joinRequest: JoinRequest = {
      type: 'join',
      gameID: this.props.gameID,
      userToken: this.props.userToken,
      playerName: this.state.username
    };
    this.props.socket.send(JSON.stringify(joinRequest));
    // disable the form inputs once the request is fired
    this.setState({disabled: true})
  }

  render() {
    return (
    <div className="login">
      <Container className='loginContainer' maxWidth='sm'>
        <Grid container direction='row' justify='center' alignItems='baseline' spacing={1}>
          <Grid item xs={7}>
            <TextField
              autoFocus
              helperText='Enter screen name'
              onChange={this.handleChange}
              disabled={this.state.disabled}
              value={this.state.username}
              fullWidth
            />
          </Grid>
          <Grid item>
            <Button variant='contained' color='primary' onClick={this.handleSubmit}>Join</Button>
          </Grid>
        </Grid>
        <Backdrop open={this.state.disabled}>
          <CircularProgress />
        </Backdrop>
      </Container>
    </div>
    );
  }
}