import React, { Component, SyntheticEvent } from 'react';
import { UIPosition, UIState } from '../ui-types';
import { Card } from '../../common/durak-types';
import PrettyCardNameUI from './pretty-card';

interface CardUIProps {
  isBot?: boolean;
  onBoard?: boolean;
  uiPosition?: UIPosition;
  ui: UIState;
  card: Card;
  onClick(event: SyntheticEvent, card: Card): void;
}

export class CardUI extends Component<CardUIProps> {
  render() {
    const classes = ["card"];
    if (this.props.isBot === true) {
      classes.push("bot");
    } else if (this.props.isBot === false) {
      classes.push("top");
    }

    if (this.props.ui.selected.some((selection: Card) => selection.id === this.props.card.id)) {
      classes.push("selected");
    }
    if (this.props.ui.hovered === this.props.card) {
      classes.push("hovered");
    }

    if (this.props.onBoard) {
      classes.push("onboard");
    } else {
      classes.push("inhand")
    }

    let style = new Object();
    if (typeof this.props.uiPosition !== 'undefined') {
      style = {
        transform: `rotate(${this.props.uiPosition.rotation}deg)`,
        top: this.props.uiPosition.yPosition,
        left: this.props.uiPosition.xPosition,
      };
    }

    return (
      <span className={classes.join(' ')} style={style} onClick={(event: SyntheticEvent) => this.props.onClick(event, this.props.card)}>
        <PrettyCardNameUI card={this.props.card} />
      </span>
    );
  }
}

interface SlotArrowProps {
  cards: Card[];
  slotIndex: number;
  uiPosition: UIPosition;
  ui: UIState;
  onSlotClick(event: SyntheticEvent, slot: number, selectedCardIndex: number[]): void;
}

export class SlotArrowUI extends Component<SlotArrowProps> {
  render() {
    // A Slot Arrow click moves all selected cards into that slot
    const classes = ["slot"];
    let slotActive = this.props.ui.selected.length > 0 ? true : false;
    if (slotActive) {
      classes.push("active");
    }

    let selectedCardIndices: number[] = [];
    this.props.ui.selected.forEach((selection: Card) => {
      selectedCardIndices.push(this.props.cards.findIndex((card: Card) => card.id === selection.id))
    })

    let style = {
      transform: `rotate(${this.props.uiPosition.rotation}deg)`,
      top: this.props.uiPosition.yPosition,
      left: this.props.uiPosition.xPosition,
    };

    const onSlotClick = (event: SyntheticEvent) => {
      this.props.onSlotClick(event, this.props.slotIndex, selectedCardIndices)     // On a valid slot click, move the selected card
    }

    return <span className={classes.join(' ')} style={style} onClick={slotActive ? onSlotClick : undefined}/>
  }
}