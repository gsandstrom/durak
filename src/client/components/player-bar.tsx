import React from 'react';
import Badge from '@material-ui/core/Badge';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import SecurityIcon from '@material-ui/icons/Security';
import SvgIcon from '@material-ui/core/SvgIcon';
import Typography from '@material-ui/core/Typography';
import { Player } from '../../common/durak-types';
import { DurakClient } from '../durak-client';
// .svg's must be imported with require() to play nicely with webpack and the loader
let HandIcon = require('../assets/hand-light.svg').default;

interface PlayerBarProps {
  game: DurakClient;
}

export function PlayerBar({game}: PlayerBarProps) {
  return (
    <div className="playerBar">
      <Grid container spacing={2} justify='center'>
        {game.state.players.map((player: Player, id: number) => {
          return (
            <Grid item key={id}>
              <Paper elevation={3} className='player'>
                <Grid container direction='column' alignItems='center' spacing={1}>
                  <Grid item className='playerName'>
                    <Typography variant='h4'>{game.isDefending(id) && <SecurityIcon color='primary'/>}{player.name}</Typography>
                  </Grid>
                  <Grid item>
                    <Badge badgeContent={player.hand.length} color='secondary' overlap='circle'>
                      <SvgIcon component={HandIcon} fontSize='large'/>
                    </Badge>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
}