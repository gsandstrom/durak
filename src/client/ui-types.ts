import { Card } from '../common/durak-types';

export interface UIPosition {
  rotation: number;
  xPosition: number;
  yPosition: number;
}

export interface UIState {
  selected?: Card[];
  hovered?: Card;
}