import * as UUID from 'uuid';
import * as WebSocket from 'websocket';
import * as Durak from '../common/durak-types';
import { RoomCache } from './room-cache';
import Debug from 'debug';
import RandomBot from './random-bot';

const debug = Debug('durak:manager');

const roomCache = new RoomCache();
const bots = new Map<string, RandomBot>();

const updateAll = (gameID: string, message?: Durak.MessagePacket) => {
  // send everyone in the game the GameState
  const room = roomCache.get(gameID);
  if(room) {
    room.sendUpdateToAllPlayers(message);
  }
}

const handleRegister = (request: Durak.Request, socket: any): void => {
  const room = roomCache.get(request.gameID);
  // if the user is already registered in the game, close their old socket
  // and register the new one.
  if (room && room.hasMember(request.userToken)) {
    debug('user ' + request.userToken + ' already registered, updating socket');
    room.updateSocket(request.userToken, socket);
    room.sendUpdateToPlayer(request.userToken);
  }
  // if the user isn't in the game, send nothing and wait for their join request
}

const handleJoin = (request: Durak.JoinRequest, socket: any): void => {
  // create game if necessary
  const room = roomCache.get(request.gameID, true);
  room.join(request.userToken, socket, request.playerName, request.bot);
  if (room.isSpectating(request.userToken)) {
    debug('user ' + request.playerName + ' joined ' + request.gameID + ' as a spectator');
    const packet: Durak.MessagePacket = {
      type: 'join-spectator',
      username: request.playerName,
    }
    updateAll(request.gameID, packet);
  } else {
    debug('user ' + request.playerName + ' joined ' + request.gameID + ' as a player');
    const packet: Durak.MessagePacket = {
      type: 'join-player',
      username: request.playerName,
    }
    updateAll(request.gameID, packet);
  }
}

const handleAddBot = (request: Durak.Request) => {
  const room = roomCache.get(request.gameID);
  if(room && room.isAuthorized(request)) {
    debug('adding bot to room ' + request.gameID);
    const token = UUID.v4();
    bots.set(token, new RandomBot('ws://localhost:3000', 'durak-protocol', token, request.gameID, () => {bots.delete(token)}));
  }
}

const handleStart = (request: Durak.Request): boolean => {
  const room = roomCache.get(request.gameID);
  if(room && room.isAuthorized(request)) {
    debug('starting new game in room ' + request.gameID);
    room.startNewGame();
    return true;
  }
  return false;
}

const handleDefend = (request: Durak.DefendRequest): boolean => {
  const { playerID, playedCard, targetCard } = request.action;
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  // const playerID = room.getPlayerID(request.userToken);
  if (game.canDefend(playedCard, targetCard, playerID) && room.isAuthorized(request)) {
    roomCache.updateGame(gameID, game.defend(request.action));
    const packet: Durak.MessagePacket = {
      type: 'defend',
      username: room.getUserName(playerID),
      card: playedCard,
      target: targetCard,
    }
    updateAll(gameID, packet);
    return true;
  } else {
    return false;
  }
}

const handleBounce = (request: Durak.BounceRequest): boolean => {
  const { playerID, playedCard } = request.action;
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  // const playerID = room.getPlayerID(request.userToken);
  if (game.canBounce(playedCard, playerID) && room.isAuthorized(request)) {
    roomCache.updateGame(gameID, game.bounce(request.action));
    const packet: Durak.MessagePacket = {
      type: 'bounce',
      username: room.getUserName(playerID),
      card: playedCard,
    }
    updateAll(gameID, packet);
    return true;
  } else {
    return false;
  }
}

const handleAttack = (request: Durak.AttackRequest): boolean => {
  const { playerID, playedCard } = request.action;
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  // const playerID = room.getPlayerID(request.userToken);
  if (game.canAttack(playedCard, playerID) && room.isAuthorized(request)) {
    const attackType = game.state.board.length === 0 ? 'first-attack' : 'attack';
    roomCache.updateGame(gameID, game.attack(request.action));
    const packet: Durak.MessagePacket = {
      type: attackType,
      username: room.getUserName(playerID),
      card: playedCard,
    }
    updateAll(gameID, packet);
    return true;
  } else {
    return false;
  }
}

const handleSurrender = (request: Durak.Request): boolean => {
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  const playerID = room.getPlayerID(request.userToken);
  if (game.canSurrender(playerID) && room.isAuthorized(request)) {
    roomCache.updateGame(gameID, game.surrender({playerID}));
    const packet: Durak.MessagePacket = {
      type: 'surrender',
      username: room.getUserName(playerID),
    }
    updateAll(gameID, packet);
    return true;
  } else {
    return false;
  }
}

const handleClear = (request: Durak.Request): boolean => {
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  if(game.canClear() && room.isAuthorized(request)) {
    const defender = room.getUserName(room.game.getDefenderID());
    roomCache.updateGame(gameID, game.clearBoard());
    const packet: Durak.MessagePacket = {
      type: 'clear',
      username: defender,
    }
    updateAll(gameID, packet);
  } else {
    return false;
  }
}

const handleUndo = (request: Durak.Request): boolean => {
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  if(game && room.isAuthorized(request)) {
    if(game.undo()) {
      roomCache.updateGame(gameID, game);
      const packet: Durak.MessagePacket = {
        type: 'undo',
      }
      updateAll(gameID, packet);
      return true;
    }
  }
  return false;
}

const handleRedo = (request: Durak.Request): boolean => {
  const gameID = request.gameID;
  const room = roomCache.get(gameID);
  const game = room.game;
  if (game && room.isAuthorized(request)) {
    if(game.redo()) {
      roomCache.updateGame(gameID, game);
      const packet: Durak.MessagePacket = {
        type: 'redo',
      }
      updateAll(gameID, packet);
      return true;
    }
  }
  return false;
}

export const manager = (ws: WebSocket.server) => {
  const allowedOriginRegexpString = process.env.ALLOWED_ORIGIN_REGEXP || '.*';
  const allowedOriginRegexp = new RegExp(allowedOriginRegexpString);
  const allowedHostRegexpString = process.env.ALLOWED_HOST_REGEXP || 'localhost:3000';
  const allowedHostRegexp = new RegExp(allowedHostRegexpString);
  debug('allowed origins: ' + allowedOriginRegexpString);

  debug('manager initialized');

  // TODO only accept connections from our site
  function originIsAllowed(origin: string) {
    return allowedOriginRegexp.test(origin);
  }

  function hostIsAllowed(host: string) {
    return allowedHostRegexp.test(host);
  }

  ws.on('request', (wsRequest: WebSocket.request) => {
    if(!originIsAllowed(wsRequest.origin) && !hostIsAllowed(wsRequest.host)) {
      wsRequest.reject();
      debug((new Date()) + ' Connection from ' + wsRequest.host + ' (origin ' + wsRequest.origin + ') rejected');
      return;
    }

    const socket: WebSocket.connection = wsRequest.accept('durak-protocol', wsRequest.origin);
    debug((new Date()) + ' Connection from ' + wsRequest.host + ' (origin ' + wsRequest.origin + ') accepted');
    socket.on('message', (data: WebSocket.IMessage) => {
      if(data.type === 'utf8') {
        const request = JSON.parse(data.utf8Data);
        if (request.type === 'register') handleRegister(request, socket);
        else if (request.type === 'join') handleJoin(request, socket);
        else if (request.type === 'defend') handleDefend(request);
        else if (request.type === 'bounce') handleBounce(request);
        else if (request.type === 'attack') handleAttack(request);
        else if (request.type === 'surrender') handleSurrender(request);
        else if (request.type === 'clear') handleClear(request);
        else if (request.type === 'undo') handleUndo(request);
        else if (request.type === 'redo') handleRedo(request);
        else if (request.type === 'start') handleStart(request);
        else if (request.type === 'addBot') handleAddBot(request);
        else debug('Unknown request format: ' + data.utf8Data);
      }
    });
  });
};
