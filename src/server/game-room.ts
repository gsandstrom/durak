import Debug from 'debug';
import { DurakAdmin } from './durak-admin';
import { Request, MessagePacket } from '../common/durak-types';
import { connection } from 'websocket';
import { shuffle } from '../common/utils';

interface RoomMember {
  socket: connection,
  playerID: number,
  userToken: string,
  name: string,
  bot: boolean
}

export class GameRoom {
  game: DurakAdmin;
  id: string;
  debug: Debug.Debugger;
  members: Map<string, RoomMember>;
  closeRoomCallback: () => void;

  constructor(id: string, closeRoomCallback: () => void) {
    this.id = id;
    this.game = new DurakAdmin();
    this.members = new Map<string, RoomMember>();
    this.closeRoomCallback = closeRoomCallback;

    this.debug = Debug('durak:game-room:' + id);
  }

  hasMember(userToken: string): boolean {
    return this.members.has(userToken);
  }

  getPlayerID(userToken: string): number {
    return this.members.get(userToken).playerID;
  }

  getUserName(playerID: number) {
    // Get Username from PlayerID. Currently assuming playerID
    // comes from an active player, not a spectator.
    let username = '';
    this.members.forEach((member: RoomMember) => {
      if (member.playerID === playerID) {
        username = member.name;
      }
    })
    return username;
  }

  isSpectating(userToken: string): boolean {
    return this.members.get(userToken).playerID < 0;
  }

  isAuthorized(request: Request): boolean {
    const { userToken, action } = request;
    // userToken must be in the game
    if(!this.members.has(userToken)) {
      console.log('unauthorized: userToken ' + userToken + ' not in game');
      return false;
    }
    // if an action is provided, the playerID must match the userToken
    if(action) {
      if(action.playerID !== this.getPlayerID(userToken)) {
        console.log('unauthorized: userToken ' + userToken + ' is not player ' + action.playerID);
        return false;
      }
    }
    return true;
  }

  join(userToken: string, socket: connection, name: string, bot=false) {
    // protect against creating multiple members with same userToken
    if (!this.hasMember(userToken)) {
      // add as spectator if the game already started
      const id = this.game.started ? -1 : this.game.addPlayerToGame(name);
      const member: RoomMember = {
        socket,
        playerID: id,
        userToken,
        name,
        bot
      };
      this.debug('adding player ' + member.name + ' ' + member.playerID);
      this.members.set(userToken, member);
      this.registerSocketDisconnectHandler(socket);
    }
  }

  updateSocket(userToken: string, socket: connection) {
    const member = this.members.get(userToken);
    // disconnect old socket
    if(member.socket) {
      member.socket.close();
    }
    member.socket = socket;
    this.registerSocketDisconnectHandler(socket);
  }

  registerSocketDisconnectHandler(socket: connection) {
    // whenever a socket disconnects, check if any non-bot member sockets are still alive
    socket.on('close', () => {
      this.debug('a user disconnected, counting dead connections')
      const humans = [...this.members.values()].filter((member: RoomMember) => !member.bot);
      // this.debug([...this.members.values()]);
      const connected = humans.find((m: RoomMember) => m.socket.connected);
      if(!connected) {
        this.debug('no remaining humans playing, closing room');
        this.closeRoomCallback();
      }
    });
  }

  sendUpdateToAllPlayers(message?: MessagePacket) {
    this.members.forEach((member: RoomMember) => {
      member.socket.send(JSON.stringify(this.game.playerGameState(member.playerID, message)));
    });
  }

  sendUpdateToPlayer(userToken: string) {
    const member = this.members.get(userToken);
    member.socket.send(JSON.stringify(this.game.playerGameState(member.playerID)));
  }

  startNewGame() {
    this.game = new DurakAdmin();

    // prune all disconnected room members
    const prunedMembers: RoomMember[] = [...this.members.values()].filter((member: RoomMember) => member.socket.connected);
    this.members = new Map<string, RoomMember>();

    // randomize order and give all members a playerID
    // (this will convert spectators to active playerss)
    shuffle(prunedMembers, Date.now());
    prunedMembers.forEach((member: RoomMember) => {
      member.playerID = this.game.addPlayerToGame(member.name);
      this.members.set(member.userToken, member);
    });

    this.game.start();
    const packet: MessagePacket = {
      type: 'start',
      username: this.getUserName(this.game.state.roundLeaderID),
      card: this.game.state.firstPantCard,
    }
    this.sendUpdateToAllPlayers(packet);
  }

  close() {
    this.members.forEach((member: RoomMember) => {
      if (member.socket.connected) member.socket.close();
    });
  }
}