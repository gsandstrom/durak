import { Card } from '../common/durak-types';
import { shuffle } from '../common/utils';
const seedSalt = 1585710368814;

export class Deck {
  cards: Card[];

  constructor(cards?: Card[]) {
    // clone input
    this.cards = [...cards];
  }

  size(): number {
    return this.cards.length;
  }

  shuffle(seed?: number) {
    if(seed === undefined){
      seed = Date.now() + seedSalt;
    }
    shuffle(this.cards, seed);
  }

  draw(): Card {
    return this.cards.pop();
  }

  discard(n: number): void {
    if (n <= 0) {
      throw new Error("discard must be called with a positive argument");
    }
    for (let i = 0; i < n; i++) {
      this.cards.pop();
    }
  }

  // helper function, search through for a matching card and pull it,
  // removing from the deck and return the Card object if there's a match
  pull(value: number, suit: string): Card {
    for (let i = 0; i < this.cards.length; i++) {
      if (this.cards[i].value === value && this.cards[i].suit === suit) {
        const card: Card = this.cards[i];
        this.cards.splice(i, 1);
        return card;
      }
    }
    throw new Error("Card not in deck: (" + value + ", " + suit + ")");
  }

  peekBottom(): Card {
    return this.cards[0];
  }
}
