import Debug from 'debug';
import { DurakAdmin } from './durak-admin';
import { GameRoom } from './game-room';

const debug = Debug('durak:room-cache');

export class RoomCache {
  map: Map<string, GameRoom>;

  constructor() {
    this.map = new Map<string, GameRoom>();
  }

  get(gameID: string, create:boolean = false): GameRoom {
    if(!this.map.has(gameID) && create) {
      console.log('creating new room ' + gameID);
      this.createRoom(gameID);
    }
    return this.map.get(gameID);
  }

  getGame(gameID: string): DurakAdmin {
    return this.map.get(gameID).game;
  }

  updateGame(gameID: string, game: DurakAdmin) {
    const room = this.map.get(gameID);
    room.game = game;
  }

  createRoom(gameID: string) {
    debug('creating room ' + gameID);
    this.map.set(gameID, new GameRoom(gameID, () => this.closeRoom(gameID)));
  }

  closeRoom(gameID: string) {
    debug('closing room ' + gameID);
    const room = this.get(gameID);
    if (room) {
      room.close();
      this.map.delete(gameID);
    } else {
      debug('cannot close room, no such game id ' + gameID);
    }
  }
}