import MersenneTwister from 'mersenne-twister';
import fs from 'fs';

export class RoomnameGenerator {
  words: string[];
  generator: MersenneTwister;
  wordsPerName: number;

  constructor(sourcepath: string, wordsPerName: number, seed = Date.now()) {
    this.generator = new MersenneTwister(seed);
    this.words = fs.readFileSync(sourcepath).toString().split('\n');
    this.wordsPerName = wordsPerName;
  }

  getName(): string {
    const wordsToUse: string[] = new Array();
    for (const _i of Array(this.wordsPerName).keys()) {
      wordsToUse.push(this.pickRandomWord());
    }
    return wordsToUse.join('-');
  }

  pickRandomWord(): string {
    const randomIndex = Math.floor(this.generator.random() * this.words.length);
    return this.words[randomIndex];
  }
}