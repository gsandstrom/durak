import { Card, CardStack, Player, GameState, PlayerGameState, MessagePacket } from '../common/durak-types';
import { Action, AttackAction, DefendAction, BounceAction } from '../common/durak-types';
import { DurakGame } from '../common/durak-game';
import { DeckBuilder } from './deck-builder';
import { Deck } from './deck';
import { History } from './history';
import rfdc from 'rfdc';
const clone = rfdc();

export class DurakAdmin extends DurakGame {
  deck: Deck;
  history: History<GameState>;
  maxHandSize: number;
  started: boolean;

  constructor() {
    const initialState: GameState = {
      deck: new Array(),
      discards: 0,
      pantCard: undefined,
      board: new Array(),
      players: new Array(),
      roundLeaderID: undefined,
      defenderID: undefined,
      firstPantCard: undefined,
    }
    super(initialState);
    this.deck = new Deck([]);
    this.history = new History<GameState>();
    this.maxHandSize = 6;
    this.started = false;
  }

  hydrate(state: GameState): DurakAdmin {
    this.state = clone(state);
    this.deck = new Deck(state.deck);
    return this;
  }

  // dump the gamestate as a simple json blob
  gameState(): GameState {
    // update deck
    this.state.deck = this.deck.cards;
    return clone(this.state);
  }

  // dump the gamestate, sanitized for a particular player
  playerGameState(playerID: number, message?: MessagePacket): PlayerGameState {
    const redactedState = this.gameState();
    redactedState.deck = new Array(this.deck.size());
    redactedState.players.forEach((player: Player) => {
      if (player.id !== playerID) {
        // don't share other players' hands
        player.hand = new Array(player.hand.length);
      };
    });
    redactedState.firstPantCard = undefined;
    const playerState: PlayerGameState = {
      ...redactedState,
      playerID,
    }
    if (message !== undefined) {
      playerState.message = message;
    }
    return playerState;
  }

  spectatorGameState(message?: MessagePacket): PlayerGameState {
    return this.playerGameState(undefined, message);
  }

  // insert a named player to the game, auto-asign an id
  addPlayerToGame(playerName: string): number {
    const id = this.state.players.length;
    const player: Player = {
      id,
      name: playerName,
      hand: new Array()
    };
    this.state.players[id] = player;
    return id;
  }

  removeFromHand (playerID: number, toRemove: Card): DurakAdmin {
    const hand = this.state.players[playerID].hand;
    const copy = hand.filter( (card: Card) => {
      return (card.id !== toRemove.id);
    });
    this.state.players[playerID].hand = copy;
    return this;
  }

  // pick a player to lead, draw the pant suit, deal cards
  start(): DurakAdmin {
    this.deck = DeckBuilder();
    this.deck.shuffle();
    this.state.pantCard = this.deck.peekBottom();
    this.state.players.forEach((player: Player) => {
      this.refillHand(player);
    });
    this.assignFirstRoundLeader();
    this.history.save(this.gameState());
    this.started = true;
    return this;
  }

  assignFirstRoundLeader(): DurakAdmin {
    // player with lowest card in pant suit goes first
    let leadingPlayerID;
    // dummy card with value higher than ACE (14)
    let lowestCard = {value: 15, suit: this.state.pantCard.suit, id: -1};
    this.state.players.forEach((player: Player) => {
      player.hand.forEach((card: Card) => {
        if (
          card.suit === this.state.pantCard.suit &&
          card.value < lowestCard.value
        ) {
          leadingPlayerID = player.id;
          lowestCard = clone(card);
        }
      });
    });
    if(typeof leadingPlayerID === 'undefined') {
      // fall back to random if nobody has a card in pant suit
      const numPlayers = this.state.players.length;
      leadingPlayerID = Math.floor(Math.random() * numPlayers);
    } else {
      this.state.firstPantCard = lowestCard;
    }
    this.state.roundLeaderID = leadingPlayerID
    this.state.defenderID = this.nextPlayer(leadingPlayerID);
    return this;
  }

  advanceDefender(): DurakAdmin {
    this.state.defenderID = this.nextPlayer(this.state.defenderID);
    return this;
  }

  refillHand(player: Player): DurakAdmin {
    while(this.deck.size() > 0 && player.hand.length < this.maxHandSize) {
      player.hand.push(this.deck.draw());
    }
    return this;
  }

  drawPhase(): DurakAdmin {
    // original round leader draws up first
    const defenderID = this.getDefenderID();
    let drawerID = this.getRoundLeaderID();
    let count = 0;
    // loop through all players and draw, starting with original round attackers,
    //  skipping defender
    do {
      if (drawerID !== defenderID) {
        this.refillHand(this.state.players[drawerID]);
      }
      drawerID = this.nextPlayer(drawerID);
      count += 1;
    } while (count < this.state.players.length);
    // defender draws last
    this.refillHand(this.state.players[defenderID]);
    return this;
  }

  attack(action: AttackAction): DurakAdmin {
    // remove played card from hand
    this.removeFromHand(action.playerID, action.playedCard);

    // add it to the board
    const stack = { bottom: action.playedCard };
    this.state.board.push(stack);
    this.history.save(this.gameState());
    return this;
  }

  defend(action: DefendAction): DurakAdmin {
    // remove played card from hand
    this.removeFromHand(action.playerID, action.playedCard);

    // add it on top of the target card
    const targetStack = this.state.board.find((stack: CardStack) => stack.bottom.id === action.targetCard.id);
    targetStack.top = action.playedCard;
    this.history.save(this.gameState());
    return this;
  }

  bounce(action: BounceAction): DurakAdmin {
    // remove played card from hand
    this.removeFromHand(action.playerID, action.playedCard);

    // add it to board
    const stack = { bottom: action.playedCard };
    this.state.board.push(stack);

    // increment defending player
    this.advanceDefender();
    this.history.save(this.gameState());
    return this;
  }

  surrender(action: Action): DurakAdmin {
    // add all cards from the board to player's hand
    const hand = this.state.players[action.playerID].hand;
    this.state.board.forEach( (stack: CardStack) => {
      hand.push(stack.bottom);
      if (stack.top) {
        hand.push(stack.top);
      }
    });

    // clear the board
    this.state.board = new Array();
    // redraw everyone's hands
    this.drawPhase();
    // next round leader is the player after whoever surrendered
    this.state.roundLeaderID = this.nextPlayer(action.playerID);
    this.state.defenderID = this.nextPlayer(this.getRoundLeaderID());
    this.history.save(this.gameState());
    return this;
  }

  clearBoard(): DurakAdmin {
    // add all cards from the board to the discard pile (should be stacks of 2)
    this.state.discards += 2 * this.state.board.length;

    this.state.board = new Array();
    this.drawPhase();
    // if whoever was defending is still in the game, they are the new leader
    const defendedID = this.getDefenderID();
    if (this.isPlayerActive(defendedID)) {
      this.state.roundLeaderID = defendedID;
    } else {
      this.state.roundLeaderID = this.nextPlayer(defendedID);
    }
    this.state.defenderID = this.nextPlayer(this.getRoundLeaderID());
    this.history.save(this.gameState());
    return this;
  }

  undo(): DurakAdmin {
    const lastState = this.history.undo();
    if (lastState) {
      this.hydrate(lastState);
      return this;
    }
    return undefined;
  }

  redo(): DurakAdmin {
    const nextState = this.history.redo();
    if (nextState) {
      this.hydrate(nextState);
      return this;
    }
    return undefined;
  }

  // helper for testing. do a deep copy of all elements.
  clone(): DurakAdmin {
    const copy = new DurakAdmin();
    copy.hydrate(this.gameState());
    return copy;
  }
}