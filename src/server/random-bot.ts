import Bot from './bot';
import { DurakGame } from '../common/durak-game';
import { PlayerGameState, Card, CardStack, DefendAction } from '../common/durak-types';
import { Request, BounceAction, AttackAction } from '../common/durak-types';

// This bot prefers to clear the board, but otherwise plays random legal moves
export default class RandomBot extends Bot {
  requestTimeout: NodeJS.Timeout;

  constructor(wsURL: string, protocols: string, userToken: string, gameID: string, onDisconnect: ()=>void) {
    super(wsURL, protocols, userToken, gameID, onDisconnect);
    this.playerName = "random-bot";
  }

  handleMessage(state: PlayerGameState): void {
    const myID = state.playerID;
    const game = new DurakGame(state);

    // bail out if we're just spectating or the game is over
    if (myID < 0 || game.isGameOver()) return;

    const myHand = state.players[myID].hand;

    clearTimeout(this.requestTimeout);

    if (game.isDefending(myID) && game.canClear()) {
      // prefer to clear
      this.queueRequest(this.assembleRequest('clear'));
    } else {
      // otherwise pick a random legal move
      const possibleRequests: Request[] = [];
      if(game.canSurrender(myID)) possibleRequests.push(this.assembleRequest('surrender'));
      myHand.forEach((card: Card) => {
        // bounce
        if (game.canBounce(card, myID)) {
          const action: BounceAction = {
            playerID: myID,
            playedCard: card
          };
          possibleRequests.push(this.assembleRequest('bounce', action));
        }
        // attack
        if (game.canAttack(card, myID)) {
          const action: AttackAction = {
            playerID: myID,
            playedCard: card
          };
          possibleRequests.push(this.assembleRequest('attack', action));
        }
        // defend
        game.state.board.forEach((stack: CardStack) => {
          if (!stack.top) {
            if (game.canDefend(card, stack.bottom, myID)) {
              const action: DefendAction = {
                playerID: myID,
                playedCard: card,
                targetCard: stack.bottom
              };
              possibleRequests.push(this.assembleRequest('defend', action));
            }
          }
        })
      });

      // pick a random move out of the hat
      const randomIndex = Math.floor(Math.random() * possibleRequests.length);
      const randomRequest = possibleRequests[randomIndex];
      if (randomRequest) {
        this.debug('Random action selected: ' + randomRequest.type + ' ' + randomRequest.action);
        this.queueRequest(randomRequest);
      } else {
        this.debug('I have no legal actions');
      }
    }
  }

  queueRequest(request: Request) {
    const noise = Math.floor(Math.random() * 1000);
    const ms = request.type === 'clear' ? 6000 : 2000;
    this.requestTimeout = setTimeout(() => {this.sendRequest(request)}, ms+noise);
  }
}