import { Card, FaceValues, Suits } from '../common/durak-types';
import { Deck } from './deck';

export interface DeckOptions {
  numberDecks: number;
  cullBelow: number;
  cullPercent: number;
}

export const DeckBuilder = (opts?: DeckOptions): Deck => {
  const generateStdCards = (startingID: number): Card[] => {
    const stdCards = [] as Card[];
    let id = startingID;
    for (const suit of Object.keys(Suits)) {
      for (const value of Object.keys(FaceValues)) {
        stdCards.push({
          'id': id,
          'value': FaceValues[value],
          'suit': Suits[suit]
        });
        id++;
      }
    }
    return stdCards;
  };

  const defaultOptions: DeckOptions = {
    numberDecks: 1,
    cullBelow: FaceValues.TWO,
    cullPercent: 0
  };

  const options: DeckOptions = {
    ...defaultOptions,
    ...opts
  };

  let cards = [] as Card[];

  // how many decks to mix in
  for (let i=options.numberDecks; i > 0; i--) {
    cards = cards.concat(generateStdCards(cards.length));
  }

  // throw away any cards below a certain value?
  cards.filter((card: Card) => { return card.value >= options.cullBelow });

  const deck = new Deck(cards);

  // throw away some percentage at random
  if (options.cullPercent > 0) {
    const cullCount = Math.round(options.cullPercent * cards.length);
    deck.shuffle();
    deck.discard(cullCount);
  }

  return deck;
};
