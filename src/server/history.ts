export class History<T> {
  history: T[];
  current: number;
  newest: number;

  constructor() {
    this.history = [];
    this.current = -1;
    this.newest = undefined;
  }

  // get current state in history
  now(): T {
    if(this.current >= 0) {
      return this.history[this.current];
    } else {
      return undefined;
    }
  }

  // add a new item to the history
  save(newItem: T): void {
    this.current += 1;
    this.newest = this.current;
    this.history[this.current] = newItem;
  }

  // step back in history, if possible
  undo(): T {
    if(this.current > 0) {
      this.current -= 1;
      return this.history[this.current];
    } else {
      return undefined;
    }
  }

  // step forwards in history, if possible
  redo(): T {
    if(this.current < this.newest) {
      this.current += 1;
      return this.history[this.current];
    } else {
      return undefined;
    }
  }
}