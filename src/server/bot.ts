import Debug from 'debug';
import * as WebSocket from 'websocket';
import { JoinRequest, PlayerGameState, Action, Request } from '../common/durak-types';

export default abstract class Bot {
  userToken: string;
  gameID: string;
  playerName: string;
  debug: Debug.Debugger;
  ws: WebSocket.connection;

  constructor(wsURL: string, protocols: string, userToken: string, gameID: string, onDisconnect: ()=>void) {
    this.debug = Debug('durak:bot:' + userToken);
    this.debug('initializing');
    this.userToken = userToken;
    this.gameID = gameID;
    this.playerName = 'bot';

    const client = new WebSocket.client();
    client.on('connectFailed', (err: Error) => {
      this.debug('could not connect to game: ' + err);
    });
    client.on('connect', (ws: WebSocket.connection) => {
      this.ws = ws;

      this.ws.on('message', (message: WebSocket.IMessage) => {
        if (message.utf8Data) {
          this.handleMessage(JSON.parse(message.utf8Data));
        }
      });

      this.ws.on('close', () => {
        this.debug('socket closed');
        onDisconnect();
      });

      const joinRequest: JoinRequest = {
        type: 'join',
        playerName: this.playerName,
        userToken: this.userToken,
        gameID: this.gameID,
        bot: true
      }
      this.ws.send(JSON.stringify(joinRequest));
    });
    client.connect(wsURL, protocols);
  }

  assembleRequest(type: string, action?: Action): Request {
    const request: Request = {
      type,
      userToken: this.userToken,
      gameID: this.gameID,
      action
    };
    return request;
  }

  sendRequest(request: Request) {
    this.ws.send(JSON.stringify(request));
  }

  abstract handleMessage(state: PlayerGameState): void;
}