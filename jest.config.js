/*
 * https://medium.com/@RupaniChirag/writing-unit-tests-in-typescript-d4719b8a0a40
 */

module.exports = {
  verbose: false,
  transform: {'^.+\\.ts?$': 'ts-jest'},
  testEnvironment: 'node',
  testRegex: '/tests/.*\\.(test|spec)?\\.(ts|tsx)$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node']
};
