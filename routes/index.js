var express = require('express');
var router = express.Router();
var path = require('path');

var RG = require('../dist/server/roomname-generator');
var gen = new RG.RoomnameGenerator(path.join(__dirname, '..', 'longwords.txt'), 3);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..', 'public', 'index.html'));
});
router.get('/game/*', function(req, res, next) {
  res.sendFile(path.join(__dirname, '..', 'public', 'game.html'));
});
router.get('/api/generate-name', function(req, res, next) {
  res.send(gen.getName());
})
module.exports = router;