const path = require('path');

module.exports = {
  entry: {
    "durak-client": "./src/client/game.tsx",
    "homepage": "./src/client/index.tsx"
  },
  mode: "production",
  watch: false,

  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",

  resolve: {
  	// Add '.ts' and '.tsx' as resolvable extensions.
    extensions: [".js", ".ts", ".tsx"]
  },

  module: {
  	rules: [
    	{
      	test: /\.ts(x?)$/,
        exclude: /node_modules/,
        use: [
        	{
          	loader: "ts-loader"
          }
        ]
      },
      // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
      {
      	enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.mp3$/,
        loader: "file-loader",
        include: path.resolve(__dirname, 'src')
      },
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        use: ['@svgr/webpack']
      }
    ]
  },

  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist', 'client'),
    publicPath: '/client/'
  },

  // When importing a module whose path matches one of the following, just
  // assume a corresponding global variable exists and use that instead.
  // This is important because it allows us to avoid bundling all of our
  // dependencies, which allows browsers to cache those libraries between builds.
  externals: {
  	"react": "React",
    "react-dom": "ReactDOM",
    "js-cookie": "Cookies"
  }
};
