import { History } from '../src/server/history';

test('history workflow', () => {
  const history = new History<number>();
  history.save(1); // [1^&]   ^: current pointer  &: end of history
  history.save(2); // [1, 2^&]
  expect(history.undo()).toBe(1); // [1^, 2&]
  expect(history.undo()).toBe(undefined); // [1^, 2&]
  expect(history.redo()).toBe(2); // [1, 2^&]
  history.undo(); // [1^, 2&]
  history.save(3); // [1, 3^&]
  history.save(4); // [1, 3, 4^&]
  expect(history.now()).toBe(4);
  history.save(5); // [1, 3, 4, 5^&]
  expect(history.undo()).toBe(4); // [1, 3, 4^, 5&]
  expect(history.undo()).toBe(3); // [1, 3^, 4, 5&]
  expect(history.undo()).toBe(1); // [1^, 3, 4, 5&]
  history.save(6); // [1, 6^&, 4, 5]
  expect(history.redo()).toBe(undefined);
});