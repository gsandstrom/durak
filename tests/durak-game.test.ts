import { DurakGame } from '../src/common/durak-game';
import { GameState, FaceValues, Suits, Card } from '../src/common/durak-types';
import { Deck } from '../src/server/deck';
import { DeckBuilder } from '../src/server/deck-builder';
import rfdc from 'rfdc';
const clone = rfdc({proto: true});

const v = FaceValues;
const s =  Suits;

const refDeck: Deck = DeckBuilder();
const refState: GameState = {
  deck: refDeck.cards,
  discards: 0,
  pantCard: refDeck.pull(v.ACE, s.SPADES),
  board: [
    {bottom: refDeck.pull(v.TWO, s.HEARTS)},
    {bottom: refDeck.pull(v.TWO, s.DIAMONDS), top: refDeck.pull(v.THREE, s.DIAMONDS)},
    {bottom: refDeck.pull(v.TWO, s.SPADES)},
    {bottom: refDeck.pull(v.TWO, s.CLUBS)}
  ],
  players: [
    {
      id: 0,
      name: 'player0',
      hand: [
        refDeck.pull(v.SEVEN, s.HEARTS),
        refDeck.pull(v.SEVEN, s.DIAMONDS),
        refDeck.pull(v.SEVEN, s.CLUBS),
        refDeck.pull(v.SEVEN, s.SPADES)
      ],
    }, {
      id: 1,
      name: 'player1',
      hand: [refDeck.pull(v.EIGHT, s.HEARTS)],
    }, {
      id: 2,
      name: 'player2',
      hand: [
        refDeck.pull(v.NINE, s.HEARTS),
        refDeck.pull(v.NINE, s.CLUBS),
        refDeck.pull(v.NINE, s.DIAMONDS),
        refDeck.pull(v.NINE, s.SPADES)
      ],
    },
  ],
  roundLeaderID: 2,
  defenderID: 0,
  firstPantCard: undefined,
};

test('cardEquals: positive test', () => {
  const cardA = clone(refState.players[0].hand[0]);
  const cardB = clone(cardA);
  const game = new DurakGame(refState);
  expect(game.cardEquals(cardA, cardB)).toBe(true);
});

test('cardEquals: negative test', () => {
  const cardA = clone(refState.players[0].hand[0]);
  const cardB = clone(refState.players[0].hand[1]);
  const game = new DurakGame(refState);
  expect(game.cardEquals(cardA, cardB)).toBe(false);
});

test('inHand: positive test', () => {
  const card = clone(refState.players[0].hand[0]);
  const game = new DurakGame(refState);
  expect(game.inHand(card, 0)).toBe(true);
});

test('inHand: player does not have card', () => {
  const card = clone(refState.players[1].hand[0]);
  const game = new DurakGame(refState);
  expect(game.inHand(card, 0)).toBe(false);
});

test('isOpen: positive test', () => {
  const card = clone(refState.board[0].bottom);
  const game = new DurakGame(refState);
  expect(game.isOpen(card)).toBe(true);
});

test('isOpen: card already covered', () => {
  const card = clone(refState.board[1].bottom);
  const game = new DurakGame(refState);
  expect(game.isOpen(card)).toBe(false);
});

test('isOpen: card not on board', () => {
  const card = clone(refState.players[0].hand[0]);
  const game = new DurakGame(refState);
  expect(game.isOpen(card)).toBe(false);
});

test('currentDefender', () => {
  const game = new DurakGame(refState);
  expect(game.getDefenderID()).toBe(0);
});

test('canDefend: positive test', () => {
  const playedCard = clone(refState.players[0].hand[0]); // 7H
  const targetCard = clone(refState.board[0].bottom); // 2H
  const game = new DurakGame(refState);
  expect(game.canDefend(playedCard, targetCard, 0)).
    toBe(true);
});

test('canDefend: out of turn', () => {
  const playedCard = clone(refState.players[1].hand[1]);
  const targetCard = clone(refState.board[0].bottom);
  const game = new DurakGame(refState);
  expect(game.canDefend(playedCard, targetCard, 2)).
    toBe(false);
});

test('canDefend: covered card', () => {
  const playedCard = clone(refState.players[0].hand[1]);
  const targetCard = clone(refState.board[1].bottom);
  const game = new DurakGame(refState);
  expect(game.canDefend(playedCard, targetCard, 0)).
    toBe(false);
});

test ('canDefend: card not in hand', () => {
  const playedCard: Card = {value: v.ACE, suit: s.HEARTS, id: 53};
  const targetCard = clone(refState.board[0].bottom); // 2H
  const game = new DurakGame(refState);
  expect(game.canDefend(playedCard, targetCard, 0)).
    toBe(false);
});

test ('canDefend: card does not beat covered card', () => {
  const playedCard = clone(refState.players[0].hand[1]); // 7D
  const targetCard = clone(refState.board[0].bottom); // 2H
  const game = new DurakGame(refState);
  expect(game.canDefend(playedCard, targetCard, 0)).
    toBe(false);
});

test('valueOnBoard: positive test', () => {
  const game = new DurakGame(refState);
  expect(game.isValueOnBoard(v.TWO)).toBe(true);
  expect(game.isValueOnBoard(v.THREE)).toBe(true);
});

test('valueOnBoard: negative test', () => {
  const game = new DurakGame(refState);
  expect(game.isValueOnBoard(v.ACE)).toBe(false);
});

test('canAttack: positive test', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  state.players[1].hand = [deck.pull(v.THREE, s.CLUBS)];
  const game = new DurakGame(state);
  expect(game.canAttack(clone(state.players[1].hand[0]), 1)).
    toBe(true);
});

test('canAttack: ok to lead', () => {
  const state = clone(refState);
  state.board = [];
  let game = new DurakGame(state);
  expect(game.canAttack(clone(state.players[2].hand[0]), 2)).
    toBe(true);
  state.roundLeaderID = 1;
  state.defenderID = 2;
  game = new DurakGame(state);
  expect(game.canAttack(clone(state.players[1].hand[0]), 1)).
    toBe(true);
});

test('canAttack: not your turn to lead', () => {
  const state = clone(refState);
  state.board = [];
  const game = new DurakGame(state);
  expect(game.canAttack(clone(state.players[1].hand[0]), 1)).
    toBe(false);
});

test('canAttack: not in hand', () => {
  // intentional playerID mismatch
  const game = new DurakGame(refState);
  expect(game.canAttack(clone(refState.players[2].hand[0]), 1)).
    toBe(false);
});

test('canAttack: defender out of cards', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  state.board.push({ bottom: deck.draw() });
  const game = new DurakGame(state);
  expect(game.canAttack(clone(state.players[1].hand[0]), 1)).
    toBe(false);
});

test('canAttack: defender tries to attack', () => {
  const game = new DurakGame(refState);
  expect(game.canAttack(clone(refState.players[0].hand[0]), 0)).
    toBe(false);
});

test('canBounce: positive tests', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  // player1 already has 8H in hand
  state.board = [
    {bottom: deck.pull(v.EIGHT, s.SPADES)}
  ];
  state.defenderID = 1;
  let game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(true);
  state.board.push({bottom: deck.pull(v.EIGHT, s.CLUBS)});
  game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(true);
});

test('canBounce: trying to bounce with a different card', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  // player1 already has 8H in hand
  state.board = [
    {bottom: deck.pull(v.TEN, s.SPADES)}
  ];
  state.defenderID = 1;
  const game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(false);
});

test('canBounce: defense already occured', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  // player1 already has 8H in hand
  state.board = [
    {bottom: deck.pull(v.EIGHT, s.SPADES), top: deck.pull(v.TEN, s.SPADES)}
  ];
  state.defenderID = 1;
  let game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(false);
  state.board.push({bottom: deck.pull(v.EIGHT, s.CLUBS)});
  game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(false);
});

test('canBounce: no attacks yet', () => {
  const state = clone(refState);
  // player1 already has 8H in hand
  state.board = [];
  state.defenderID = 1;
  const game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(false);
});

test('canBounce: not the defender', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  // player1 already has 8H in hand
  state.board = [
    {bottom: deck.pull(v.EIGHT, s.SPADES)}
  ];
  let game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(false);
  state.board.push({bottom: deck.pull(v.EIGHT, s.CLUBS)});
  game = new DurakGame(state);
  expect(game.canBounce(clone(state.players[1].hand[0]), 1)).toBe(false);
});

test('canBounce: card not in hand', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  state.board = [
    {bottom: deck.pull(v.EIGHT, s.SPADES)}
  ];
  const game = new DurakGame(state);
  expect(game.canBounce(deck.pull(v.EIGHT, s.CLUBS), 0)).toBe(false);
});

test('canBounce: next defender has too few cards', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  state.board = [
    {bottom: deck.pull(v.TWO, s.HEARTS)}
  ];
  state.players[0].hand = [deck.pull(v.TWO, s.SPADES)];
  state.players[1].hand = [deck.pull(v.THREE, s.HEARTS)]; // only one card in hand
  const game = new DurakGame(state);
  expect(game.canBounce(state.players[0].hand[0], 0)).toBe(false);
})

test('canSurrender: positive test', () => {
  const game = new DurakGame(refState);
  expect(game.canSurrender(0)).toBe(true);
});

test('canSurrender: not your turn', () => {
  const game = new DurakGame(refState);
  expect(game.canSurrender(1)).toBe(false);
});

test('canSurrender: no attack yet', () => {
  const state = clone(refState);
  state.board = [];
  const game = new DurakGame(state);
  expect(game.canSurrender(0)).toBe(false);
});

test('canClear: positive test', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  state.board = [
    {bottom: deck.pull(v.TWO, s.HEARTS), top: deck.pull(v.THREE, s.HEARTS)},
    {bottom: deck.pull(v.TEN, s.SPADES), top: deck.pull(v.JACK, s.SPADES)}
  ];
  const game = new DurakGame(state);
  expect(game.canClear()).toBe(true);
});

test('canClear: undefended cards', () => {
  const state = clone(refState);
  const deck = DeckBuilder();
  state.board = [
    {bottom: deck.pull(v.TWO, s.HEARTS), top: deck.pull(v.THREE, s.HEARTS)},
    {bottom: deck.pull(v.TEN, s.SPADES)}
  ];
  const game = new DurakGame(state);
  expect(game.canClear()).toBe(false);
});

test('canClear: empty board', () => {
  const state = clone(refState);
  state.board = [];
  const game = new DurakGame(state);
  expect(game.canClear()).toBe(false);
});

test('countOpenStacks', () => {
  const game = new DurakGame(refState);
  expect(game.countOpenStacks()).toBe(3);
});

const twoClubs: Card = {
  value: v.TWO,
  suit: s.CLUBS,
  id: 0
};
const threeClubs: Card = {
  value: v.THREE,
  suit: s.CLUBS,
  id: 1
};
const twoDiamonds: Card = {
  value: v.TWO,
  suit: s.DIAMONDS,
  id: 2
};
const twoSpades: Card = {
  value: v.TWO,
  suit: s.SPADES,
  id: 3
}
const tenDiamonds: Card = {
  value: v.TEN,
  suit: s.DIAMONDS,
  id: 4
};

test('beats: same suit positive test', () => {
  const state = clone(refState);
  state.pantCard = twoClubs;
  let game = new DurakGame(state);
  expect(game.cardBeats(threeClubs, twoClubs)).toBe(true); // pant suit clubs
  state.pantCard = twoSpades;
  game = new DurakGame(state);
  expect(game.cardBeats(threeClubs, twoClubs)).toBe(true); // pant suit spades
});

test('beats: same suit negative test', () => {
  const state = clone(refState);
  state.pantCard = twoDiamonds;
  let game = new DurakGame(state);
  expect(game.cardBeats(twoDiamonds, tenDiamonds)).toBe(false); // pant suit diamonds
  state.pantCard = twoSpades;
  game = new DurakGame(state);
  expect(game.cardBeats(twoDiamonds, tenDiamonds)).toBe(false); // pant suit spades
});

test('beats: different suit no pants', () => {
  const state = clone(refState);
  state.pantCard = twoSpades;
  let game = new DurakGame(state);
  expect(game.cardBeats(tenDiamonds, threeClubs)).toBe(false); // pant suit spades
  expect(game.cardBeats(twoClubs, tenDiamonds)).toBe(false);
});

test('beats: different suit pant card', () => {
  const state = clone(refState);
  state.pantCard = twoDiamonds;
  let game = new DurakGame(state);
  expect(game.cardBeats(tenDiamonds, threeClubs)).toBe(true); // pant suit diamonds
  expect(game.cardBeats(twoDiamonds, threeClubs)).toBe(true);
  state.pantCard = twoClubs;
  game = new DurakGame(state);
  expect(game.cardBeats(twoClubs, tenDiamonds)).toBe(true);
});