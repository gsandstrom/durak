import * as Durak from '../src/common/durak-types';
import { DurakAdmin } from '../src/server/durak-admin';
import { DeckBuilder } from '../src/server/deck-builder';

const v = Durak.FaceValues;
const s =  Durak.Suits;

// Setup known gamestate
const refState: Durak.GameState = {
  deck: [],
  discards: 0,
  pantCard: undefined,
  board: [],
  players: [],
  roundLeaderID: undefined,
  defenderID: undefined,
  firstPantCard: undefined,
}

const deck = DeckBuilder();

refState.pantCard = deck.pull(v.ACE, s.SPADES);

const board = refState.board;
board.push( {bottom: deck.pull(v.TWO, s.HEARTS)} );
board.push( {bottom: deck.pull(v.TWO, s.DIAMONDS), top: deck.pull(v.THREE, s.DIAMONDS)} );
board.push( {bottom: deck.pull(v.TWO, s.SPADES)} );
board.push( {bottom: deck.pull(v.TWO, s.CLUBS)} );
board.push( {bottom: deck.pull(v.NINE, s.SPADES)} );

const players =refState.players;
players[0] = {
  id: 0,
  name: 'player0',
  hand: [
    deck.pull(v.SEVEN, s.HEARTS),
    deck.pull(v.SEVEN, s.DIAMONDS),
    deck.pull(v.SEVEN, s.CLUBS),
    deck.pull(v.SEVEN, s.SPADES)
  ]
};
players[1] = {
  id: 1,
  name: 'player1',
  hand: [
    deck.pull(v.EIGHT, s.HEARTS)
  ]
};
players[2] = {
  id: 2,
  name: 'player2',
  hand: [
    deck.pull(v.NINE, s.HEARTS),
    deck.pull(v.NINE, s.CLUBS),
    deck.pull(v.NINE, s.DIAMONDS)
  ]
};

refState.roundLeaderID = 0;
refState.defenderID = 1;

refState.deck = deck.cards;

// create and hydrate a GameAdmin with this state
const game: DurakAdmin = new DurakAdmin();
game.hydrate(refState);
// End setup

test('playerGameState', () => {
  const p0 = game.playerGameState(0);
  expect(p0.playerID).toEqual(0);
  expect(p0.players[0].hand.length).toEqual(refState.players[0].hand.length);
  expect(p0.players[0].hand[0]).toBeDefined();
  expect(p0.players[1].hand.length).toEqual(refState.players[1].hand.length);
  expect(p0.players[1].hand[1]).toBeUndefined();
  expect(p0.players[2].hand.length).toEqual(refState.players[2].hand.length);
  expect(p0.players[2].hand[2]).toBeUndefined();

  const p2 = game.playerGameState(2);
  expect(p2.playerID).toEqual(2);
  expect(p2.players[0].hand.length).toEqual(refState.players[0].hand.length);
  expect(p2.players[0].hand[0]).toBeUndefined();
  expect(p2.players[1].hand.length).toEqual(refState.players[1].hand.length);
  expect(p2.players[1].hand[1]).toBeUndefined();
  expect(p2.players[2].hand.length).toEqual(refState.players[2].hand.length);
  expect(p2.players[2].hand[2]).toBeDefined();
});

test('defend', () => {
  const action = {
    gameID: 0,
    playerID: 0,
    playedCard: refState.players[0].hand[0],
    targetCard: refState.board[0].bottom
  };

  const result = game.clone();
  result.defend(action);
  // card was put in the right place
  expect(result.state.board[0].top.id).
    toEqual(action.playedCard.id);
  // card was removed from hand
  expect(result.inHand(action.playedCard, action.playerID)).
    toBe(false);
  // same player is still defending
  expect(result.getDefenderID()).toBe(game.getDefenderID());
  // target card is still in the same place
  expect(result.state.board[0].bottom).
    toStrictEqual(game.state.board[0].bottom);
});

test('attack', () => {
  const action = {
    gameID: 0,
    playerID: 2,
    playedCard: game.state.players[2].hand[0]
  };

  const result = game.clone();
  result.attack(action);
  // card was put on the table
  expect(result.isOpen(action.playedCard)).toBe(true);
  // card no longer in hand
  expect(result.inHand(action.playedCard, action.playerID)).toBe(false);
  // same player is still defending
  expect(result.getDefenderID()).toBe(game.getDefenderID());
});

test('bounce', () => {
  const result = game.clone();
  result.state.board = [
    {bottom: result.deck.pull(v.ACE, s.HEARTS)},
    {bottom: result.deck.pull(v.ACE, s.CLUBS)}
  ];
  result.state.players[2].hand = [
    result.deck.pull(v.ACE, s.DIAMONDS)
  ];  
  result.state.roundLeaderID = 1;
  result.state.defenderID = 2;

  const action = {
    gameID: 0,
    playerID: 2,
    playedCard: game.state.players[2].hand[0]
  };

  result.bounce(action);
  // card was put on the table
  expect(result.isOpen(action.playedCard)).toBe(true);
  // card no longer in bouncer's hand
  expect(result.inHand(action.playedCard, action.playerID)).toBe(false);
  // attacker and defender increment, round leader stays the same
  expect(result.getDefenderID()).toBe(0);
  expect(result.state.roundLeaderID).toBe(1);
});

test('surrender', () => {
  const action = {
    gameID: 0,
    playerID: 0
  };

  const result = game.clone();
  result.surrender(action);
  // no cards on the board
  expect(result.state.board.length).toBe(0);
  // correct number of cards in hand
  expect(result.state.players[0].hand.length).toBe(10);
  // player that surrendered doesn't get to lead next
  expect(result.getDefenderID()).toBe(2);
  expect(result.getRoundLeaderID()).toBe(1);
});