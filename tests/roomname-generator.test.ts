import { RoomnameGenerator } from '../src/server/roomname-generator';

test('known seed', () => {
  const gen = new RoomnameGenerator('./longwords.txt', 3, 0);
  expect(gen.pickRandomWord()).toBe('methodical');
  expect(gen.getName()).toBe('noviciate-rattail-stephenson');
})