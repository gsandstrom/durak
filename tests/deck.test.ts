import { Deck } from '../src/server/deck';
import { DeckBuilder } from '../src/server/deck-builder';
import { FaceValues, Suits } from '../src/common/durak-types';
import * as hash from 'object-hash';

test('default opts yield a standard deck', () => {
  const deck: Deck = DeckBuilder();
  expect(hash.sha1(deck.cards)).toBe('4946b393650c4200c4e5eff5770d100bee8304c3');
//  expect(deck.cards[0].value).toBe(FaceValues.TWO);
//  expect(deck.cards.length).toBe(52);
});

test('shuffle with known seed', () => {
  const deck = DeckBuilder();
  deck.shuffle(0);
  expect(hash.sha1(deck.cards)).toBe('59fbad13a50620fbe9c2768a3a66d173231fe76e');
});

test('draw a card', () => {
  const deck = DeckBuilder();
  expect(hash.sha1(deck.draw())).toBe(hash.sha1({id: 51, suit: Suits.DIAMONDS, value: FaceValues.ACE}));
  expect(deck.cards.length).toBe(51);
});
